package com.selbot.amazon;

import org.openqa.selenium.By;

import com.selbot.testng.api.base.Annotations;

public class HomePage extends Annotations{
	
	By bySignIn = By.id("nav-link-accountList-nav-line-1");
	
	public LoginPage clickSignIn() {
		
		click(getWebElement(bySignIn));
		return new LoginPage();
	}

}
