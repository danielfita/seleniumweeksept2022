package com.selbot.amazon;

import org.openqa.selenium.By;

import com.selbot.testng.api.base.Annotations;

public class LoginPage extends Annotations{
	
	By userName = By.id("ap_email");
	By password = By.id("ap_password");
	By continueButton = By.id("continue");
	By submitButton = By.id("signInSubmit");
	
	public void signIn(String uName, String pwd) {
		
		clearAndType(getWebElement(userName), uName);
		click(getWebElement(continueButton));
		clearAndType(getWebElement(password), pwd);
		click(getWebElement(submitButton));

	}

	

}
