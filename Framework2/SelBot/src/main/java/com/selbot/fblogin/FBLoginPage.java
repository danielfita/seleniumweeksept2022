package com.selbot.fblogin;

import org.openqa.selenium.By;

import com.selbot.testcases.FB_TC_001;
import com.selbot.testng.api.base.Annotations;

public class FBLoginPage extends Annotations{
		
		By mail = By.id("email");
		By pass = By.id("pass");
		By btn = By.name("login");
		
		public void login(String email,String password) {
		
			clearAndType(getWebElement(mail), email);
			clearAndType(getWebElement(pass), password);
			click(getWebElement(btn));
			
		}

		

}
