package com.selbot.opencart.loginpage;

import org.openqa.selenium.By;

import com.selbot.testng.api.base.Annotations;

public class HomePage extends Annotations{
	
	By welcomeNote = By.xpath("//*[text()='Please confirm who you are!']");
	
	public HomePage checkWelcomeNote() {
		verifyDisplayed(getWebElement(welcomeNote));
		return this;
	}

}
