package com.selbot.opencart.loginpage;

import org.openqa.selenium.By;

import com.selbot.testng.api.base.Annotations;

public class LoginPage extends Annotations{
	
	By userName = By.id("input-email");
	By password = By.id("input-password");
	By loginBtn = By.xpath("(//*[text()='Login'])[2]");
	
	
	public LoginPage enterUserName(String un)
	{
		clearAndType(getWebElement(userName), un);
		return this;
	}
	
	public LoginPage enterPassword(String pwd)
	{
		clearAndType(getWebElement(password), pwd);
		return this;
	}
	
	public HomePage clickLoginBtn()
	{
		click(getWebElement(loginBtn));
		return new HomePage();
	}
	


}
