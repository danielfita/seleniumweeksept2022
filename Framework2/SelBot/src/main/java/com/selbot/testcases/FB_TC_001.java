package com.selbot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.selbot.fblogin.FBLoginPage;
import com.selbot.testng.api.base.Annotations;

public class FB_TC_001 extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "Facebook_TC_001";
		testcaseDec = "Login Verify";
		author = "Krithi";
		category = "smoke"; 
		excelFileName = "FB_TC_001";
	}
	
	
	@Test
	
	public void tc1() {
		
		FBLoginPage fblog = new FBLoginPage();
		fblog.login("krithi@gmail.com","qwerty123");
	}

}
