package com.selbot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.selbot.amazon.HomePage;
import com.selbot.testng.api.base.Annotations;

public class AmazonSignin extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "Amazon_TC_001";
		testcaseDec = "Login Verify";
		author = "Deeksit";
		category = "smoke"; 
		excelFileName = "Amazon_TC1";
	}
	
	@Test(dataProvider="fetchData")
	public void testCase1(String un, String pwd) {
		
		HomePage hp = new HomePage();
		hp.clickSignIn().signIn(un,pwd);
	}

}
