package com.selbot.testcases;

import static org.testng.Assert.assertEquals;

import java.sql.Driver;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.selbot.testng.api.base.Annotations;

public class Amazon_tc extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "AZ_TC_001";
		testcaseDec = "Login Verify";
		author = "Daniel";
		category = "smoke"; 
		excelFileName = "OC_TC_001";
	}

	@Test(enabled=false)
	public void atodaydeal() {
		locateElement("xpath", "//a[contains(text(),\"Today's Deals\")]").click();
		String title = getCuttentDriver().getTitle();
		assertEquals(title, "Amazon.in Great Indian Festival 2022");
		
	}
	
	@Test
	public void blockbusterdeals() throws InterruptedException {
		locateElement("xpath", "//a[contains(text(),\"Today's Deals\")]").click();
		locateElement("xpath","//span[text()='Blockbuster Deals']").click();
		Thread.sleep(2000);
		locateElement("xpath","//span[text()='Under ₹500']").click();
		Thread.sleep(2000);
		locateElement("xpath","//div[text()='Puzzles, Plush & More: Amazon Brands & More']").click();
		Thread.sleep(2000);
		List<WebElement> toys = locateElements("xpath", "//div[@id='octopus-dlp-asin-stream']");
		for(WebElement toy : toys) {
			if(toy.getText().contains("PLAY POCO 3 in 1 Magnet Set - Alphabets and Numbers with 26 Capital Letter, 26 Small Letter, 30 Number Magnets and Magnetic Board - Child Safe Foam Magnets - Ideal for 3 4 5 Year Old Boys and Girls")) {
				Thread.sleep(2000);
				toy.click();
				break;
			}
			//System.out.println(toy.getText());
		}
	
		
	}

}
