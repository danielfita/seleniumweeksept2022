package com.selbot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.selbot.opencart.loginpage.LoginPage;
import com.selbot.testng.api.base.Annotations;

public class OC_TC_5 extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "OC_TC_001";
		testcaseDec = "Login Verify";
		author = "Daniel";
		category = "smoke";
		excelFileName = "OC_TC_001";
	} 
	
	@Test(dataProvider="fetchData")
	public void login(String uName, String pwd){
		
		new LoginPage()
			.enterUserName(uName)
			.enterPassword(pwd)
			.clickLoginBtn();
		
	}
	

}
