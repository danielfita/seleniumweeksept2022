package com.week2.day4;

import java.util.ArrayList;

public class DynamicArray {
	
	public static void main(String[] args) {
		
		
		
		ArrayList emp1 = new ArrayList();
		
		emp1.add("Ram");
		
		emp1.add(123);
		emp1.add(1000.00);
		emp1.add('M');
		emp1.add(true);
		
		System.out.println(emp1);
		System.out.println(emp1.size());
		
		emp1.remove(2);
		
		System.out.println(emp1);
		System.out.println(emp1.size());
		
		emp1.add("Chennai");
		
		System.out.println(emp1);
		System.out.println(emp1.size());
		
		emp1.add(2, 10000.00);
		System.out.println(emp1);
		System.out.println(emp1.size());
		
		emp1.clear();
		System.out.println(emp1);
		System.out.println(emp1.size());

		
		
	}

}
