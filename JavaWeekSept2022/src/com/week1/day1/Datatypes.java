package com.week1.day1;

public class Datatypes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//primitive 
			//1. Boolean - boolean
			//2. Numeric 
					//1. character - char
					//2. Integer - byte, short, int, long
					//3. floating point - float, double
			
		//non primitive
		 // class, interface, Arrays, String
		
		//boolean (1 bit)
		boolean switchs = true;
		boolean test = false;
		
		System.out.println(test);
		
		//char (2 byte)
		char letter = 'r';
		System.out.println(letter);
		
		//byte (1 byte) -128 to 127
		byte weekNum = 6;
		System.out.println(weekNum);
		
		//short (2 byte) - -32*** to 32***
		short calenderDays = 366;
		System.out.println(calenderDays);
		
		//int (4 byte) - 
		int number = 987128871;
		System.out.println(number);
		
		//long (8 byte)
		long population = 87234009748374l;
		System.out.println(population);
		
		//float(4 byte)
		float salary = 890770000000000000000000.00f;
		System.out.println(salary);
		
		//double (8 byte)
		double distance = 98172398798127239123123.73;
		System.out.println(distance);
		
		
		

	}

}
