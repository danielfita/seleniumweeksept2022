package com.week1.day2;

public class Decement {

	public static void main(String[] args) {
		// post decrement a-- - a=a-1

		int a = 5;
		a--;
		System.out.println(a); // 4
		System.out.println(a--); // 4
		System.out.println(a); // 3

		System.out.println("-----------------------");
		// pre decrement --b - b=b-1

		int b = 5;
		--b;
		System.out.println(b); //  4
		System.out.println(--b); //  3
		System.out.println(b); // 3

	}

}
