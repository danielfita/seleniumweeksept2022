package com.week1.day2;

public class Increment {

	public static void main(String[] args) {

		//post increment a++ - a=a+1
		
		int a = 1;
		a++;
		System.out.println(a); //2
		System.out.println(a++); //2
		System.out.println(a); //3
		
		System.out.println("-----------------------");
		// pre increment ++b - b=b+1
		
		int b= 1;
		++b;
		System.out.println(b); //2
		System.out.println(++b); //3
		System.out.println(b); //3

	}

}
