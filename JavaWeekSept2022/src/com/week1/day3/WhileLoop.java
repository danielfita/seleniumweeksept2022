package com.week1.day3;

public class WhileLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int a = 1;
		
		while(a<=10)
		{
			System.out.println(a);
			a++;
		}
		
		System.out.println("-----------------------------");
		a--;
		while(a>=1)
		{
			System.out.println(a);
			a--;
		}

	}

}
