package com.week6.day1;

public interface IndianMedical {
	
	public void oncology();
	public void physio();
	
	public static void billing() {
		
		System.out.println("IndianMedical static- billing");
		
	}
	
	default void insurance() {
		System.out.println("IndianMedical default- insurance");

	}
	
	

}
