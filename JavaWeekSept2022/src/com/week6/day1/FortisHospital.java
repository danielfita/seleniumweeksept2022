package com.week6.day1;

public class FortisHospital implements IndianMedical,USMedical,NewUKMedical{

	@Override
	public void oncology() {

		System.out.println("FortisHospital - oncology");
	}

	@Override
	public void physio() {

		System.out.println("FortisHospital - physio");

	}

	@Override
	public void ENT() {
		System.out.println("FortisHospital - ENT");
		
	}

	@Override
	public void pediatrician() {
		System.out.println("FortisHospital - pediatrician");
		
	}

	@Override
	public void cardia() {
		System.out.println("FortisHospital - cardia");
		
	}

	@Override
	public void neurology() {
		System.out.println("FortisHospital - neurology");
		
	}

}
