package com.week3.day4;

import java.util.ArrayList;

public class DynamicArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList emp = new ArrayList();
		
		ArrayList<String> a = new ArrayList<>();

		emp.add("Ram");
		emp.add(134);
		emp.add("Chennai");
		emp.add(true);
		emp.add('M');

		System.out.println(emp.size());

		for (int i = 0; i < emp.size(); i++) {
			System.out.println(emp.get(i));
		}

		System.out.println("_______________________");
		emp.add(1000.50);
		System.out.println(emp.size());

		for (int i = 0; i < emp.size(); i++) {
			System.out.println(emp.get(i));
		}

		System.out.println("_______________________");

		emp.remove("Chennai");
		System.out.println(emp.size());

		for (int i = 0; i < emp.size(); i++) {
			System.out.println(emp.get(i));
		}

		System.out.println("_______________________");
		emp.add(2, "Bangalore");
		System.out.println(emp.size());

		for (int i = 0; i < emp.size(); i++) {
			System.out.println(emp.get(i));
		}
	}

}
