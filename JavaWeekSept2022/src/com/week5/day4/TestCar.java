package com.week5.day4;

public class TestCar {
	
	Car car1;

	public static void main(String[] args) {
		
		
		
		Benz benz = new Benz();
		
		benz.Start();
		benz.Transmission();
		benz.AutoDrive();
		benz.Stop();
		benz.WheelDrive();
		
		System.out.println("--------------------");
		
		Swift swift = new Swift();
		
		swift.Start();
		swift.Transmission();
		swift.Stop();
		swift.WheelDrive();
		
		System.out.println("--------------------");

		//Top casting
		Car car = new Benz();
		car.Start();
		car.Transmission();
		car.Stop();
		car.WheelDrive();
		car.cruise();
		
		
		System.out.println("--------------------");
		//Down casting
//		Benz n1 = (Benz) new Car();
		//Top casting
		Vehicle vehicle = new Benz();
		vehicle.WheelDrive();
		
		TestCar tc = new TestCar();
		
		
		tc.car1 = new Benz();
		
		tc.car1 = new Swift();
		
		System.out.println("--------------------");

		car = new Swift();
		car.cruise();
		
	}

}
