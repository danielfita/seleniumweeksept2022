package com.week5.day4;

public class Benz extends Car {

	public void Start() {

		System.out.println("Benz - Keyless Start");

	}

	public void Transmission() {

		System.out.println("Benz - Automatic Transmission");

	}
	
	public void AutoDrive() {
		
		System.out.println("Benz - AutoDrive");

	}

	@Override
	public void cruise() {
		System.out.println("Benz - cruise");
		
	}

}
