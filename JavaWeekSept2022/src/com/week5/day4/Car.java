package com.week5.day4;

public abstract class Car extends Vehicle{

	public void Start() {
		
		System.out.println("Car - Start");

	}

	public void Stop() {
		
		System.out.println("Car - Stop");


	}

	public void Transmission() {
		
		System.out.println("Car - Manual Transmission");


	}
	
	public abstract void cruise();

}
