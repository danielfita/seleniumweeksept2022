package com.week5.day5;

public class Incrementer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// a++ - a=a+1;
		
		// Post increment
		
		int a = 1;
		a++;
		System.out.println(a); //2
		System.out.println(a++); //2
		System.out.println(a); //3
		System.out.println(a++); //3
		
		System.out.println("---------------------------");
		
		// pre increment
		
		int b = 1;
		++b;
		System.out.println(b); //2
		System.out.println(++b); // 3
		System.out.println(b); // 3
		System.out.println(++b); //4
	}

}
