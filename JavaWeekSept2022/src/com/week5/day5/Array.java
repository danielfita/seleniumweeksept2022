package com.week5.day5;

public class Array {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] a = new int[5];
		
		a[0] = 6;
		a[1] = 3;
		a[2] = 9;
		a[3] = 8;
		a[4] = 6;
		
		System.out.println(a[3]); //8
		System.out.println(a.length); //5
		
		System.out.println("-------------------");

		
		for(int i=0;i<a.length;i++)
		{
			System.out.println(a[i]);
		}
		
		System.out.println("-------------------");

		int[] b = {7,5,4,0,6,9};
		
		for(int i=0;i<b.length;i++)
		{
			System.out.println(b[i]);
		}
		

	}

}
