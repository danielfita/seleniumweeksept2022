package com.week4.day5;

public class Employee {
	
	String name;
	int empId;
	double salary;
	char gender;
	String city;
	boolean isActive;
	
	public Employee() {
		System.out.println("I'm from constructor Employee");
	}
	
	public Employee(String name) {
		System.out.println("I'm from constructor Employee name = " + name);
	}

	public Employee(String name, int empId, char gender, String city, boolean isActive) {
		this.name = name;
		this.empId = empId;
		this.gender = gender;
		this.city = city;
		this.isActive = isActive;
	}
	
	

}
