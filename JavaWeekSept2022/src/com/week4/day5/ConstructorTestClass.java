package com.week4.day5;

public class ConstructorTestClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Employee emp1 = new Employee("Daniel", 198, 'M', "Chennai", true);
		Employee emp2 = new Employee("Ram", 199, 'M', "Bangalore", true);		
		Employee emp3 = new Employee("Seeta", 200, 'F', "Pune", false);


		System.out.println(emp1.name+ " "+emp1.empId+ " "+emp1.gender+ " "+emp1.city+ " "+emp1.isActive);
		System.out.println(emp2.name+ " "+emp2.empId+ " "+emp2.gender+ " "+emp2.city+ " "+emp2.isActive);
		System.out.println(emp3.name+ " "+emp3.empId+ " "+emp3.gender+ " "+emp3.city+ " "+emp3.isActive);


	}

}
