package com.week4.day4;

public class Methods {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Methods m = new Methods();
		m.add();
		m.add("Reena", 6787685987698769887l);

//		Methods.multiplyTwoNum(2, 3);
		int a = m.subractTwoNum();
//		System.out.println(a);
//		System.out.println(m.subTwoNum(9, 5));
	}
	
	//no argument no return type
	public void add() {
		int a,b,c;
		a=5;
		b=2;
		c=a+b;
		System.out.println(c);
		
	}
	
	public void add(int a,int b) {
		int c;
		c=a+b;
		System.out.println(c);
		
	}
	
	public void add(String a, String b) {
		String c;
		c=a+b;
		System.out.println("I'm "+c);
		
	}
	
	public void add(String a, long b) {
		String c;
		c=a+b;
		System.out.println(c);
		
	}

	
	//with argument no return type
	public static void multiplyTwoNum(int a,int b) {
		int c;
		c= a*b;
		
		System.out.println(c);
		
		
	}
	
	//no argument with return type
	public int subractTwoNum() {
		int a = 3,b = 1,c;
		c= a-b;
		
		return c;
	}
	
	//with argument with return type
	public int subTwoNum(int a,int b) {
		int c;
		c=a-b;
		
		return c;
	}
	



}
