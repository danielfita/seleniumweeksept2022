package com.common.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverFactory {
	
	static WebDriver driver;
	
	public void launchDriver(String browser)
	{
		switch(browser)
		{
		case "chrome": WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		break;
		
		case "ff": WebDriverManager.firefoxdriver().setup();
		driver = new FirefoxDriver();
		break;
		
		case "ie": WebDriverManager.iedriver().setup();
		driver = new InternetExplorerDriver();
		break;
		
		default: System.out.println("Please provide correct browser name");
		}
	}

}
