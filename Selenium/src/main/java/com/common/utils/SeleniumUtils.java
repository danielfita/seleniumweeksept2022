package com.common.utils;

import org.openqa.selenium.By;

public class SeleniumUtils extends DriverFactory {

	public void startBrowser(String browser) {

		try {
			launchDriver(browser);
			driver.manage().window().maximize();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void launchUrl(String url) {

		try {
			if (url.equals("") && url == null) {
				System.out.println("URL is null");
			} else if (!url.contains("https") || !url.contains("http")) {
				System.out.println("URL doesn't contain http/https");
			} else {
				driver.get(url);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void closeBrowser() {
		try {
			Thread.sleep(2000);
			driver.quit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void inputData(By ele, String data) {
		try {
			driver.findElement(ele).sendKeys(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void click(By ele) {
		try {
			driver.findElement(ele).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
