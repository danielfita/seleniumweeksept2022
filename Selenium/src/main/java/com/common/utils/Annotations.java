package com.common.utils;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class Annotations extends SeleniumUtils{
	
	@BeforeMethod
	public void beforeMethod()
	{
		startBrowser("chrome");
		launchUrl("https://www.opencart.com/index.php?route=account/login");
	}
	
	@AfterMethod
	public void afterMethod()
	{
		closeBrowser();
	}

	

}
