package com.week1.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class OpenCartLogin {

	// Locator
	// 1. id
	// 2. name
	// 3. class name
	// 4. xpath
	// 5. linktext
	// 6. partial linktext
	// 7. css selector
	// 8. tag name
	
	String browser = "ff";
	WebDriver driver;

	@Test
	public void login() {
		if(browser.equals("chrome"))
		{
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		}
		else if(browser.equals("ff"))
		{
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.get("https://www.opencart.com/index.php?route=account/login");
		
//		driver.findElementById("input-email").sendKeys("daniel@gmail.com");
//		driver.findElementById("input-password").sendKeys("123455667");
//		driver.findElementByXPath("//button[text()='Login']").click();
		
		driver.findElement(By.id("input-email")).sendKeys("daniel@gmail.com");
		driver.findElement(By.id("input-password")).sendKeys("8769876897");
		driver.findElement(By.xpath("//button[text()='Login']")).click();
		
	}

}
