package com.week1.day3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Checkbox {
	
	@Test
	public void checkboxoptions()
	{
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://the-internet.herokuapp.com/checkboxes");
		WebElement checkbox1 = driver.findElement(By.xpath("//input[@type = 'checkbox'][1]"));
		WebElement checkbox2 = driver.findElement(By.xpath("//input[@type = 'checkbox'][2]"));
		if (!checkbox1.isSelected())
		{
			checkbox1.click();
		}
		if (!checkbox2.isSelected())
		{
			checkbox2.click();
		}
		
		
		
	}

}
