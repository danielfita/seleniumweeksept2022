package com.week4.day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WebTableHandling {
	@Test
	public void tables() {
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.techlistic.com/p/demo-selenium-practice.html");
		List<WebElement> header = driver.findElements(By.xpath("//table[@id='customers']//th"));
		for(WebElement eachheader:header) {
			System.out.println(eachheader.getText());
		}
		List<WebElement> data=driver.findElements(By.xpath("//table[@id='customers']//td"));
		for(WebElement eachdata:data) {
			System.out.println(eachdata.getText());
		}
		
		
	}

}
