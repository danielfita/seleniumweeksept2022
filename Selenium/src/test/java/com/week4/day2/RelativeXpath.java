package com.week4.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class RelativeXpath {
	
	@Test
	public void tablehanding()
	{
		
	WebDriverManager.chromedriver().setup();
	ChromeDriver driver = new ChromeDriver();
	driver.get("https://www.techlistic.com/p/demo-selenium-practice.html");
		driver.manage().window().maximize();
		String company = "Meta";
	String eleCompany = driver.findElement(By.xpath("//table[@id='customers']//td[text() = '"+company+"']")).getText();
	List<WebElement> columns = driver.findElements(By.xpath("//table[@id='customers']//td[text() = '"+company+"']/following-sibling::td"));
	System.out.print("\tCompany =" + eleCompany);
	String contact = columns.get(0).getText();
	String country = columns.get(1).getText();
	System.out.print("\tContact =" + contact);
	System.out.print("\tCountry =" + country);
	System.out.println();
	}

}
