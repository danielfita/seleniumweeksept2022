package com.week4.day4;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Pagination {

	ChromeDriver driver;

	@Test
	public void wtTagName() {

		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://selectorshub.com/xpath-practice-page/");
		driver.manage().window().maximize();
//		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		String country="Russia";
		By byCountry = By.xpath("//td[text()='"+country+"']");
		
		while(true) {			
		
		if(isElementPresent(byCountry)) {
			System.out.println(driver.findElement(byCountry).getText());
			break;
		}
		else
		{
			By byNext = By.xpath("//*[text()='Next']");
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(byNext));
			WebElement eleNext = driver.findElement(byNext);
			String eleNextClassAttribute = eleNext.getAttribute("class");
			if(eleNextClassAttribute.contains("disabled"))
			{
				System.err.println("The given element is not available");
				break;
			}
			else
			{
				eleNext.click();
			}
			
		}
		}
	}

	public boolean isElementPresent(By by) {

		try {
			Thread.sleep(2000);
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(by));
			if (driver.findElement(by).isDisplayed())
				return true;
			else
				return false;

		} catch (Exception e) {

			return false;
		}
	}

}
