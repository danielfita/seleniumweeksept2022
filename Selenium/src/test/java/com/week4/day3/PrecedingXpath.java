package com.week4.day3;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class PrecedingXpath {

	@Test
	public void xpath()
	{
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://cosmocode.io/automation-practice-webtable/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String country = "Gabon";
		driver.findElement(By.xpath("//strong[text() = '"+country+"']/..//preceding-sibling::td/input")).click();
//      //strong[text() = 'Albania']/ancestor::table  ------>to find the ancestor elements
		//strong[text() = 'Albania']/parent::td   ----------> to find the parent elements
		//strong[text() = 'Albania']/../child::strong
	}
}
