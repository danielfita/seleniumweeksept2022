package com.week4.day5;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class Reports {
	
	@AfterSuite
	public void afterSuite() {
		System.out.println("printing afterSuite");
	}
	
	@BeforeSuite
	public void beforeSuite() {
		System.out.println("printing beforeSuite");

	}

}
