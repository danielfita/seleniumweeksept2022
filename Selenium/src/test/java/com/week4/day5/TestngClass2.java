package com.week4.day5;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestngClass2  extends Annotations {
	
	@BeforeTest
	public void zbeforeTest() {
		System.out.println("printing beforeTest2");

	}
	
	@BeforeTest
	public void beforeTest3() {
		System.out.println("printing beforeTest3");

	}
	
	@Test
	public void aMethod4() {
		System.out.println("printing method1");
	}
	
	@Test
	public void cMethod5() {
		System.out.println("printing method2");
	}

	@Test
	public void bMethod6() {
		System.out.println("printing method3");
	}

}
