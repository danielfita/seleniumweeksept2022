package com.week4.day5;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestngClass extends Annotations {

	@BeforeTest
	public void beforeTest() {
		System.out.println("printing beforeTest1");

	}

	@DataProvider(name = "nameData")
	public Object[] fetchData() {

		Object[] o = new Object[2];
		o[0] = "Rama";
		o[1] = "Ravi";

		return o;

	}

	@Test(dataProvider = "nameData", priority = 1, dependsOnMethods = "cMethod2")
	public void aMethod1(String name) {
		System.out.println("printing method1 -- " + name);
	}

	@Test(invocationCount = 3, priority = 2)
	public void cMethod2() {
		System.out.println("printing method2");
	}

	@Test(priority = 3)
	public void bMethod3() {
		System.out.println("printing method3");
	}

}
