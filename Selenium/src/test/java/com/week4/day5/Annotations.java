package com.week4.day5;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class Annotations extends Reports{
	
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("printing before method");
	}
	
	@AfterMethod
	public void afterMethod() {
		System.out.println("printing after method");
	}
	
	@BeforeClass
	public void beforeClass() {
		System.out.println("printing before class");
	}
	
	@AfterClass
	public void afterClass() {
		System.out.println("printing after class");
	}

}
