package com.opencart.pages;

import org.openqa.selenium.By;

import com.common.utils.SeleniumUtils;

public class LoginPage extends SeleniumUtils{
	
	By un = By.id("input-email");
	By pwd = By.id("input-password");
	By lgnBtn = By.xpath("//button[text()='Login']");
	
	public void login() {
		
		inputData(un, "daniel@gmail.com");
		inputData(pwd, "8769876897");
		click(lgnBtn);
		
		
	}
	


}
