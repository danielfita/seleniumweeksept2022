package com.week2.day4;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AlertClass {
	
	@Test
	public void handleAlert() {

		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://the-internet.herokuapp.com/javascript_alerts");
		driver.findElement(By.xpath("//button[text()='Click for JS Alert']")).click();
			
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
		// - https://admin:admin@the-internet.herokuapp.com/basic_auth
		
	}

}
