package com.week3.day1;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Frames {

	@Test
	public void frametext() {
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://the-internet.herokuapp.com/nested_frames");
		
		driver.switchTo().frame("frame-top");
		driver.switchTo().frame("frame-middle");
		
		String text = driver.findElement(By.id("content")).getText();
		System.out.println(text);
		driver.switchTo().parentFrame();
		driver.switchTo().frame("frame-left");
		String text2 = driver.findElement(By.xpath("//body")).getText();
		System.out.println(text2);
	}
}

