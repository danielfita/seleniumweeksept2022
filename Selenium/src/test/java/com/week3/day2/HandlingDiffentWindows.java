package com.week3.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class HandlingDiffentWindows {
	
	
	@Test
	public void winHandle() throws InterruptedException {		
		
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.facebook.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		String currentWin = driver.getWindowHandle();
		
		driver.findElement(By.xpath("//a[text()='Instagram']")).click();
		
		Set<String> allWin = driver.getWindowHandles();
		List<String> allWinList = new ArrayList<String>();
		
		allWinList.addAll(allWin);
		
		System.out.println(currentWin);
		System.out.println(allWin);
		System.out.println(allWinList);

		System.out.println(driver.getTitle());
		
		driver.switchTo().window(allWinList.get(1));
		
		System.out.println(driver.getTitle());
		
		for(String eachWin:allWin) {
			String title = driver.switchTo().window(eachWin).getTitle();
			System.out.println(title);
			
			if(title.equals("Instagram")) break;
			
			
		}
		
//		Thread.sleep(2000);
		
		driver.findElement(By.name("username")).sendKeys("917zdfbxfgfthgnertzwe6276298");
		driver.findElement(By.name("password")).sendKeys("mhgmhhg");
		
		
		driver.switchTo().window(currentWin);
		allWinList.removeAll(allWin);

		driver.findElement(By.xpath("//a[text()='Portal']")).click();

		
		allWin = driver.getWindowHandles();
//		allWinList.addAll(allWin);
		
		for(String eachWin:allWin) {
			String title = driver.switchTo().window(eachWin).getTitle();
			System.out.println(title);
			
			if(title.equals("Meta Portal - Video Calling Devices with Alexa Built-in | Meta Store")) break;
			
			
		}
		
		driver.findElement(By.xpath("//span[text()='Support']/../..")).click();

		
		
		
		
		
		
		
		
		
		
		
		
	}

}
