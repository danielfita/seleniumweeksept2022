package com.week3.day4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class dragAndDropClass {
	
	@Test
	public void dragAndDrop() {
		
		
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://jqueryui.com/droppable/");
		
		WebElement frame = driver.findElement(By.xpath("//*[@class='demo-frame']"));

		
		driver.switchTo().frame(frame);
		
		WebElement draggable = driver.findElement(By.id("draggable"));
		WebElement droppable = driver.findElement(By.id("droppable"));
		Actions builder = new Actions(driver);
		
//		builder.clickAndHold(blockA).moveToElement(blockB).build().perform();
		
		builder.clickAndHold(draggable).moveToElement(droppable).release().perform();
		
	}

}
