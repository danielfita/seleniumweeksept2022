package com.week3.day4;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class HoverClass {
	
	@Test
	public void hover()
	{
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://mrbool.com/");
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		WebElement menu = driver.findElement(By.xpath("//*[@class='menulink']"));
//		WebElement menuCourses = driver.findElement(By.linkText("COURSES"));
		
		Actions builder = new Actions(driver);
		builder.moveToElement(menu).perform();
		
		driver.findElement(By.linkText("COURSES")).click();
		
		

	}

}
